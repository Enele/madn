package spieler

import ()


//func New(name string, r,g,b uint8) *data


type Spieler interface {
	
	GibName() (name string)
	SetName(name string)
	
	GibNummer () (n uint8)
	
	GibFarbe() (ro,gr,bl uint8)
	SetFarbe(ro,gr,bl uint8)
	
	// Darstellen()
}
