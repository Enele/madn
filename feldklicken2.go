package main

import (
		"fmt"
		"gfx"
		. "./spielsteine"
		sp "./spielfelder"
		"time"
		"math/rand"
		)

func view_komponente (feld sp.Spielfeld, st []Spielstein){
	gfx.Fenster(640,540)
	//Zeichnen des Spielfeldes
	feld.Darstellen()
	gfx.Archivieren()
	//Zeichnen der Spielsteine
	for i:=0; i<len(st); i++ {
		st[i].Darstellen()
	}
}

// Es folgt die CONTROL-Komponente 1 ---- Maussteuerung------		
func maussteuerung(feld sp.Spielfeld,st []Spielstein){
	for {
			t,s,mx,my := gfx.MausLesen1()
			time.Sleep(1)
			
			if t == 1 && s==1 {
				fmt.Println(mx,my);
				//jetz muss das Feld der Spielsteine durchgegangen werden und es wird geprüft, ob eins angeklickt wurde
				for i:=0; i<len(st); i++ {
					x,y := st[i].GibKoordinaten()
					h,b := st[i].GibHB()
					fmt.Println("Koordinaten",x,y,"Nummer des Steins:",st[i].GibNummer())
					if (x <= mx && mx <= x+b) && (y <= my && my <= y+h) {
						gfx.Restaurieren(x,y,30,30)
						x,y = feld.GibWeg()[(st[i].GibStart()+st[i].GibSchritte()+1)%40][1],feld.GibWeg()[(st[i].GibStart()+st[i].GibSchritte()+1)%40][0]
						st[i].SetKoordinaten(x*30,y*30)
						st[i].SetSchritte(st[i].GibSchritte()+1)
						st[i].Darstellen()
					}
				} 
			}
		}
}


func main () {
	const anzahlSpielsteine = 4
	var st []Spielstein
	
	//Hilfsvariablen
	var x,y uint16
	
	//Ein Spielfeld initiieren und erzeugen
	var feld sp.Spielfeld
	feld = sp.New()
	
	//jetzt werden vier Spielsteine erzeugt. Diese liegen in einem Feld.
	fmt.Println("neuen Spielstein erzeugen.")
	for i:=0; i<anzahlSpielsteine; i++ {
		x = uint16(rand.Int31n(400))
		y = uint16(rand.Int31n(600))
		st = append( st, New(x,y,30,30))
		st[i].SetSpieler(uint8(i+1))
		st[i].SetStart(uint8(i*10))
		
	}
	
	//Spielstein in Feld setzen --- dient nur dem Test
	for i:=0; i<anzahlSpielsteine; i++ {
		x,y = feld.GibWeg()[(st[i].GibStart()+st[i].GibSchritte())%40][1],feld.GibWeg()[(st[i].GibStart()+st[i].GibSchritte())%40][0]
		fmt.Println(x,y)
		st[i].SetKoordinaten(x*30,y*30)
	}
	
	go view_komponente(feld, st)
	go maussteuerung(feld,st)

A:	for {
		t,gedrueckt,_ := gfx.TastaturLesen1()
		if gedrueckt==1 && t == 32 {break A}
	}
}

	
	



