package main

import (
		"fmt"
		"gfx"
		. "./spielsteine"
		sp "./spielfelder"
		"time"
		"math/rand"
		)
		



func ende () {
	var t uint16
	for {
	t,_,_ = gfx.TastaturLesen1()
	if t == 32 {end = true}
}
}
var end bool

func main () {
	//Kordinaten für die Abfrage der Mausposition beim Klick
	var mx,my uint16
	var t uint8
	var s int8
	//Variablen für die Spielsteine und die Bezeichnung der Spielsteine
	//var s1,s2,s3,s4 Spielstein
	const anzahlSpielsteine = 4
	var st [anzahlSpielsteine]Spielstein
	
	//Hilfsvariablen
	var x,y,h,b uint16
	
	//Variablen für das Spielfeld
	var feld sp.Spielfeld
	var pos [40][2] uint16 = [40][2]uint16 {{5,3},{5,4},{5,5},{5,6},{5,7},{4,7},{3,7},{2,7},{1,7},{1,8},
											{1,9},{2,9},{3,9},{4,9},{5,9},{5,10},{5,11},{5,12},{5,13},{6,13},
											{7,13},{7,12},{7,11},{7,10},{7,9},{8,9},{9,9},{10,9},{11,9},{11,8},
											{11,7},{10,7},{9,7},{8,7},{7,7},{7,6},{7,5},{7,4},{7,3},{6,3}}
	
	
	//jetzt werden vier Spielsteine erzeugt. Diese liegen in einem Feld.
	fmt.Println("neuen Spielstein erzeugen.")
	for i:=0; i<anzahlSpielsteine; i++ {
		x = uint16(rand.Int31n(400))
		y = uint16(rand.Int31n(600))
		
		st[i] = New(x,y,30,30)
		st[i].SetSpieler(uint8(i+1))
		st[i].SetStart(uint8(i*10))
		
	}
	
	//Spielstein in Feld setzen --- dient nur dem Test
	for i:=0; i<anzahlSpielsteine; i++ {
		x,y = pos[(st[i].GibStart()+st[i].GibSchritte())%40][1],pos[(st[i].GibStart()+st[i].GibSchritte())%40][0]
		fmt.Println(x,y)
		st[i].SetKoordinaten(x*30,y*30)
	}
	
	//jetzt wird ein Spielfeld erzeugt
	feld = sp.New()
	
	
	gfx.Fenster(640,540)
	//Zeichnen des Spielfeldes
	feld.Darstellen()
	gfx.Archivieren()
	//Zeichnen der Spielsteine
	for i:=0; i<anzahlSpielsteine; i++ {
		st[i].Darstellen()
	}
	//x,y = s1.GibKoordinaten()
	h,b = 30,30
	//fmt.Println(x,y,h,b)
	go ende()
	for !end {
		for  !end{
			t,s,mx,my = gfx.MausLesen1()
			time.Sleep(1)
			
			if t == 1 && s==1 {
				fmt.Println(mx,my);
				//jetz muss das Feld der Spielsteine durchgegangen werden und es wird geprüft, ob eins angeklickt wurde
				for i:=0; i<anzahlSpielsteine; i++ {
					x,y = st[i].GibKoordinaten()
					fmt.Println("Koordinaten",x,y,"Nummer des Steins:",st[i].GibNummer())
					if (x <= mx && mx <= x+b) && (y <= my && my <= y+h) {
						gfx.Restaurieren(x,y,30,30)
						x,y = pos[(st[i].GibStart()+st[i].GibSchritte()+1)%40][1],pos[(st[i].GibStart()+st[i].GibSchritte()+1)%40][0]
						st[i].SetKoordinaten(x*30,y*30)
						st[i].SetSchritte(st[i].GibSchritte()+1)
						st[i].Darstellen()
					}
				} 
			}
		}
	}
	
	//gfx.TastaturLesen1()
}

	
	


