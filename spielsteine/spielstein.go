package spielsteine

import ()



//func New(x,y,h,b uint8) *data


type Spielstein interface {
	
	GibKoordinaten() (x,y uint16)
	SetKoordinaten(x,y uint16)
	
	GibHB () (h,b uint16)
	
	GibNummer () (n uint8)
	
	GibSchritte () uint8
	SetSchritte (schritte uint8) 
	
	//GibFarbe() (ro,gr,bl uint8)
	//SetFarbe(ro,gr,bl uint8)
	
	GibSpieler () (spieler uint8)
	SetSpieler (spieler uint8)
	
	IstImHaus () bool
	//SetStart(start uint8)
	//GibStart() uint8
	
	Darstellen()
}
