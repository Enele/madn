package spielsteine

import (
		"gfx"
		".././spieler"
		)

var nummer uint8 = 0 //Hierbei handelt es sich um eine nur für das Paket globale Variable. Sie ist nur für das Paket sichtbar und ermöglicht es Spielsteine eindeutig zu kennzeichenen.

type data struct {
	nummer uint8
	spieler uint8
	//start uint8
	schritte uint8
	x,y uint16
	h,b uint16
	//ro,gr,bl uint8
	imHaus bool
}



func New (x,y,h,b uint16) *data {
	var s *data = new(data)
	(*s).nummer = nummer; nummer++; //es wird die aktuelle Nummer des Paketes zugewiesen und dies anschließend erhöht
	//Zuweisung der Position und der Größe
	(*s).x = x
	(*s).y = y
	(*s).h = h
	(*s).b = b
	//die Frabe wird erst mal auf schwarz gesetzt. Diese kann dann für die Spieler angepasst werden.
	//(*s).ro = 0
	//(*s).gr = 0
	//(*s).bl = 0
	return s
}

///******* Setter ------ *************
//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) SetKoordinaten (x,y uint16) {
	(*s).x = x
	(*s).y = y
}

/*
//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) SetFarbe (ro,gr,bl uint8) {
	(*s).ro = ro
	(*s).gr = gr
	(*s).bl = bl
}
*/

//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) SetSpieler (spieler uint8) {
	(*s).spieler = spieler

}
/*
//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) SetStart (start uint8) {
	(*s).start = start

}
*/

//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) SetSchritte (schritte uint8) {
	(*s).schritte = schritte

}

///******* Getter ------ *************
//Vor.: -
//Eff.: -
//Erg.: Die aktuellen Koordinaten sind geliefert.
func (s *data) GibNummer () (n uint8) {
	return (*s).nummer
}

//Vor.: -
//Eff.: -
//Erg.: Die aktuellen Koordinaten sind geliefert.
func (s *data) GibKoordinaten () (x,y uint16) {
	return (*s).x, (*s).y
}

//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) GibHB () (h,b uint16) {
	return (*s).h, (*s).b
}
/*
//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) GibFarbe () (ro,gr,bl uint8) {
	return (*s).ro,(*s).gr,(*s).bl
}
*/

//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) GibSpieler () (spieler uint8) {
	return (*s).spieler
}

/*
//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) GibStart () (start uint8) {
	return (*s).start
}*/

//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) GibSchritte () (schritte uint8) {
	return (*s).schritte
}

//
//
//
func (s *data) IstImHaus () bool{
	return (*s).imHaus 
}
///******* Darstellen *************
//Vor.: -
//Eff.: -
//Erg.: -
func (s *data) Darstellen () {
	ro, gr,bl := (*s).spieler.GibFarbe()
	gfx.Stiftfarbe(ro,gr,bl)		//die Frabe kann dem Struct entnommen werden
	gfx.Vollrechteck((*s).x, (*s).y,(*s).b, (*s).h)
}

