package spielfelder

import () 


// New (anzSpieler uint8) *data 

type Spielfeld interface {
	
	// Test
	// Erg.: Eine Liste von x,y - Koordinaten der linken oberen Ecke aller 
	//     Felder, die zum Spielweg gehören, ist geliefert.
	//GibWeg() [][2]uint16
	
	GibAnzSpieler() uint8
	
	// Vor.: Die Nummer des Spielers existiert.
	// Erg.: Liste der x,y - Koordinaten der Startpositionen ist geliefert.
	GibPottPos(nummer uint8) [4][2]uint16
	
	// Vor.: Die Nummer des Spielers existiert.
	// Erg.: Liste der Koordinaten der Hausfelder ist geliefert.
	GibHausPos (nummer uint8) [4][2]uint16
	 
	// Vor.: Die Nummer des Spielers existiert.
	// Erg.: Die x,y -Koordinaten des Beginnfeldes  sind geliefert.
	GibSpartPos (nummer uint8) [2]uint16

	Darstellen ()
	
}

